import { Injectable } from "@angular/core";
import {
  CameraPreviewOptions,
  CameraPreview,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import { Platform } from "@ionic/angular";
import { CameraPreviewPreviewSizeDimension } from "cordova-plugin-camera-preview";

@Injectable({
  providedIn: "root"
})
export class CameraService {
  picture: string;
  isCameraOn: Boolean;
  windowWidth: number;
  windowHeight: number;
  previewSize: CameraPreviewPreviewSizeDimension;
  cameraInfo: CameraInfo = {
    zoom: null,
    maxZoom: null
  };
  cameraPreviewOpts: CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: this.windowWidth,
    height: this.windowHeight,
    camera: "rear",
    tapPhoto: false,
    previewDrag: true,
    toBack: true,
    alpha: 1
  };

  constructor(private cameraPreview: CameraPreview, platform: Platform) {
    platform.ready().then((readySource)=> {
      this.windowHeight = platform.height();
      this.windowWidth = platform.width();
    })
  }

  async startCam(cameraPreviewOpts?: CameraPreviewOptions): Promise<void> {
    this.isCameraOn = true;
    this.picture = null;
    let response = null
    if (cameraPreviewOpts){
      response = await this.cameraPreview.startCamera(cameraPreviewOpts);
    }else {
      response = await this.cameraPreview.startCamera(this.cameraPreviewOpts);
    }
    console.log("start camera: ", response);
    [this.cameraInfo.maxZoom, this.cameraInfo.zoom] = await Promise.all([
      await this.cameraPreview.getMaxZoom(),
      await this.cameraPreview.getZoom()
    ]);
    // await this.cameraPreview.setZoom(1);
  }

  async tapToFocus(evt: MouseEvent): Promise<void> {
    const [x, y] = [evt.x, evt.y];
    if (this.isCameraOn) {
      await this.cameraPreview.tapToFocus(x, y);
    }
  }

  async switchCamera(): Promise<void> {
    const result = await this.cameraPreview.switchCamera();
    console.log("switch camera: ", result);
  }

  async takePic(pictureOpts: CameraPreviewPictureOptions): Promise<string> {
    console.log("takePic() called");
    const base64String = await this.cameraPreview.takePicture(pictureOpts);
    console.log("takePic() ", {result: base64String});
    this.picture = "data:image/jpeg;base64," + base64String;
    console.log("picture: ", this.picture);
    await this.stopCam();
    return this.picture;
  }

  async takeSnapShot(pictureOpts: CameraPreviewPictureOptions): Promise<string> {
    const base64String = await this.cameraPreview.takeSnapshot(pictureOpts);
    console.log("snapshot(): ", base64String);
    this.picture = "data:image/jpeg;base64," + base64String;
    console.log("picture: ", this.picture);
    await this.stopCam()
    return this.picture;
  }

  async stopCam(): Promise<void> {
    this.isCameraOn = false;
    console.log("stopCam() called");
    // Stop the camera preview
    const result = await this.cameraPreview.stopCamera();
    console.log("stopCame(): ", result);
  }
}

export interface CameraInfo {
  zoom: number;
  maxZoom: number;
}

export interface ImageBase64 {
  base64String: string;
}

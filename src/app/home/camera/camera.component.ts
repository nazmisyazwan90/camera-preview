import { Component, OnInit } from "@angular/core";
import {
  CameraPreviewOptions,
  CameraPreviewPictureOptions,
  CameraPreview
} from "@ionic-native/camera-preview/ngx";
import { NavController } from "@ionic/angular";
import { CameraService } from 'src/app/services/camera.service';

@Component({
  selector: "app-camera",
  templateUrl: "./camera.component.html",
  styleUrls: ["./camera.component.scss"]
})
export class CameraComponent implements OnInit {
  isCameraOn: boolean = false;
  cameraPreviewOpts: CameraPreviewOptions = {
    // x: window.screen.width - window.screen.width * 0.85,
    // y: window.screen.height - window.screen.height * 0.85,
    x: 0,
    y: 0,
    // width: window.screen.width * 0.7,
    // height: window.screen.height * 0.7,
    width: window.screen.width,
    height: window.screen.height,
    camera: "rear",
    tapPhoto: false,
    previewDrag: true,
    toBack: true,
    alpha: 1
  };

  // picture options
  pictureOpts: CameraPreviewPictureOptions = {
    width: window.screen.width * 0.5,
    height: window.screen.width * 0.5,
    quality: 50
  };

  picture: string;
  maxZoom: any;
  zoom: any;

  constructor(
    private navController: NavController,
    private cameraPreview: CameraPreview,
    private cameraService: CameraService
  ) {}

  async ngOnInit() {
    await this.cameraService.startCam(this.cameraPreviewOpts);
    console.log('cam started');
  }

  async back(): Promise<void> {
    if(this.cameraService.isCameraOn){
      await this.cameraService.stopCam();
    }
    this.navController.navigateBack('');
  }

  async focusTap(evt: MouseEvent){
    await this.cameraService.tapToFocus(evt);
  }

  async takePic(): Promise<void> {
    console.log("takePic() called");
    const imgSrc: string = await this.cameraService.takePic(this.pictureOpts);
    this.navController.back();
  }
}

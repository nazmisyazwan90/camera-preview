import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import { NavController } from "@ionic/angular";
import { CameraService, CameraInfo } from "../services/camera.service";
import { CameraPreviewPreviewSizeDimension } from "cordova-plugin-camera-preview";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit, OnDestroy {
  isCameraOn: boolean = false;
  optionList: OptionList[] = [];
  windowWidth: number = window.screen.width;
  windowHeight: number = window.screen.height;
  cameraStat: CameraInfo;
  cameraPreviewOpts: CameraPreviewOptions = {
    // x: window.screen.width - window.screen.width * 0.85,
    // y: window.screen.height - window.screen.height * 0.85,
    x: 0,
    y: 0,
    // width: window.screen.width * 0.7,
    // height: window.screen.height * 0.7,
    width: this.windowWidth,
    height: this.windowHeight,
    camera: "rear",
    tapPhoto: false,
    previewDrag: true,
    toBack: true,
    alpha: 1
  };

  // picture options
  cameraPreviewPictureOpts: CameraPreviewPictureOptions = {
    // width: this.windowWidth,
    // height: this.windowHeight,
    // quality: 100
  };

  previewSize: CameraPreviewPreviewSizeDimension = {
    width: window.screen.width * 0.9,
    height: window.screen.height * 0.9
  };

  picture: string;
  maxZoom: any;
  zoom: any;

  constructor(
    private navController: NavController,
    public cameraService: CameraService
  ) {
    this.optionList = [
      {
        label: "Start Camera",
        call: "start"
      },
      {
        label: "Take Picture",
        call: "pic"
      },
      {
        label: "Take Snapshot",
        call: "snapshot"
      },
      {
        label: "Stop Camera",
        call: "stop"
      },
      {
        label: "Switch Cam",
        call: "switch"
      },
      {
        label: "Camera",
        call: "camera"
      }
    ];
  }

  async ngOnInit() {
    // const result = await this.cameraPreview.setPreviewSize(this.previewSize);
  }

  async ngOnDestroy() {
    if (this.cameraService.isCameraOn){
      await this.cameraService.stopCam();
    }
  }

  ionViewDidEnter() {
    if (this.cameraService.picture) {
      this.picture = this.cameraService.picture;
    }
  }

  async doSomething(func: OptionList, evt?: Event): Promise<void> {
    try {
      switch (func.call) {
        case "start":
          if (this.picture){
            this.picture = null;
          }
          await this.cameraService.startCam(this.cameraPreviewOpts);
          break;
        case "stop":
          if (this.cameraService.isCameraOn){
            await this.cameraService.stopCam();
          }
          break;
        case "pic":
          this.picture = await this.cameraService.takePic(this.cameraPreviewPictureOpts);
          break;
        case "snapshot":
          this.picture = await this.cameraService.takeSnapShot(this.cameraPreviewPictureOpts);
          break;
        case "switch":
          await this.cameraService.switchCamera();
          break;
        case "camera":
          if (this.cameraService.isCameraOn){
            await this.cameraService.stopCam();
          }
          await this.navController.navigateForward("home/camera");
          break;
      }
    }catch(err){
      console.log('homeComponent: ' + err);
    }
  }

  async tapToFocus(evt: MouseEvent) {
    await this.cameraService.tapToFocus(evt);
  }

  checkEvent(evt: MouseEvent) {
    console.log(evt);
  }
}

interface OptionList {
  label: string;
  call: CameraPreviewFunction;
}

type CameraPreviewFunction =
  | ""
  | "start"
  | "stop"
  | "snapshot"
  | "pic"
  | "switch"
  | "camera";

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { SafeHtmlPipe } from '../pipes/safe-html.pipe';
import { CameraComponent } from './camera/camera.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'camera',
        component: CameraComponent
      }
    ])
  ],
  declarations: [HomePage, CameraComponent, SafeHtmlPipe]
})
export class HomePageModule {}

import { SafeHtmlPipe } from './safe-html.pipe';
import { TestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';

describe('SafeHtmlPipe', () => {
  let pipe: SafeHtmlPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SafeHtmlPipe,
        { provide: DomSanitizer }
      ]
    });

    pipe = TestBed.get(SafeHtmlPipe);
  });
  
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
